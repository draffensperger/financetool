# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'active_record/fixtures'

Encoding.default_external = "UTF-8"
Encoding.default_internal = "UTF-8"

fixtures_dir = "#{Rails.root}/db/fixtures"
Dir.foreach(fixtures_dir) do |fname|
  next if fname == '.' or fname == '..'
  fname = fname.chomp(File.extname(fname) )  
  ActiveRecord::Fixtures.create_fixtures(fixtures_dir, fname)
end