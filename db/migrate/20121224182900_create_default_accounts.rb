class CreateDefaultAccounts < ActiveRecord::Migration
  def change
    create_table :default_accounts do |t|
      t.integer :account_id

      t.timestamps
    end
  end
end
