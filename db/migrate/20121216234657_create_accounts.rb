class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :name
      t.references :account_type

      t.timestamps
    end
    add_index :accounts, :account_type_id
  end
end
