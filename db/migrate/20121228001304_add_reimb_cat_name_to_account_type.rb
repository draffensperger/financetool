class AddReimbCatNameToAccountType < ActiveRecord::Migration
  def change
    add_column :account_types, :reimb_cat_name, :string
  end
end
