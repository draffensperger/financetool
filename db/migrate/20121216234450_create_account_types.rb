class CreateAccountTypes < ActiveRecord::Migration
  def change
    create_table :account_types do |t|
      t.string :name
      t.boolean :show_negated

      t.timestamps
    end
  end
end
