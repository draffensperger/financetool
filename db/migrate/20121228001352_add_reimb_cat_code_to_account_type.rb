class AddReimbCatCodeToAccountType < ActiveRecord::Migration
  def change
    add_column :account_types, :reimb_cat_code, :string
  end
end
