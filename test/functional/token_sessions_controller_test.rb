require File.dirname(__FILE__) + "/../test_helper"

class TokenSessionsControllerTest < ActionController::TestCase
	def test_auth_login
		@request.env["devise.mapping"] = Devise.mappings[:user]
		post :create
		# Assert nothing in response		
		assert_response 401
				
		email = 'testtoken@t.com'
		pw = 'secrettest'
		user = User.create()
		user.email = email
		user.password = pw 
  	user.save!
		
		post :create, {email: email, password: 'wrong'}
    assert_response 401
		
		post :create, {email: email, password: pw}
		assert_response :success
		body = JSON.parse(response.body)
		auth_token = body['auth_token']
		assert(auth_token != nil);
		
		post :create, {email: email, password: 'wrong'}
	  assert_response 401
	  
	  delete :destroy, {auth_token: 'bad token'}
	  assert_response 404
	  
	  delete :destroy, {auth_token: auth_token}
	  assert_response :success
	  body = JSON.parse(response.body)
		assert(body['auth_token'] == auth_token);
	  
	  #xxx add test for destroy token
  end
end
