require File.dirname(__FILE__) + "/../test_helper"

class AccountsControllerTest < ActionController::TestCase
  setup do
    @account = accounts(:johnbank)
  end
	
	#test "require login" do	
	def test_require_login
		Rails.logger.debug 'hello'
	
		get :index
		assert_redirected_to new_user_session_path
		
		get :new
		assert_redirected_to new_user_session_path
		
		get :show, id: @account
		assert_redirected_to new_user_session_path
		
		get :edit, id: @account
		assert_redirected_to new_user_session_path
		
		put :update, id: @account, account: {  }
		assert_redirected_to new_user_session_path
		
		assert_no_difference('Account.count') do
			post :create, account: {  }
			assert_redirected_to new_user_session_path
		end
		
		assert_no_difference('Account.count') do
			delete :destroy, id: @account
			assert_redirected_to new_user_session_path
		end
	end
	
  def test_should_get_index
		sign_in users(:john)
    get :index
    assert_response :success
    assert(assigns(:accounts).include? accounts(:johnbank))
		assert(assigns(:accounts).include? accounts(:johnbudget))
		assert_equal(2, assigns(:accounts).length)
		sign_out users(:john)
		
		sign_in users(:bob)
		get :index
    assert_response :success
    assert(assigns(:accounts).include? accounts(:bobbank))
		assert(assigns(:accounts).include? accounts(:bobbudget))
		assert_equal(2, assigns(:accounts).length)
		sign_out users(:bob)
		
		sign_in users(:jane)
		get :index
		assert_response :success
		assert_equal(0, assigns(:accounts).length)
  end  

  test "should get new" do
		sign_in users(:john)
    get :new
    assert_response :success
  end

	test "should create account" do	
		sign_in users(:john)
    assert_difference('Account.count') do
      post :create, account: {  }
    end

    assert_redirected_to account_path(assigns(:account))
		
		# Assert that there is now a new account showing in the index
		get :index
		assert_equal(3, assigns(:accounts).length)
		sign_out users(:john)
		
		sign_in users(:bob)
		get :index
    assert_response :success
    assert(assigns(:accounts).include? accounts(:bobbank))
		assert(assigns(:accounts).include? accounts(:bobbudget))
		assert_equal(2, assigns(:accounts).length)
  end

  test "should show account" do
		sign_in users(:john)
    get :show, id: accounts(:johnbank)
		assert_response :success
		assert_equal(accounts(:johnbank), assigns(:account))    
		
		get :show, id: accounts(:bobbank)
		
  end

  test "should get edit" do
    # get :edit, id: @account
    # assert_response :success
  end

  test "should update account" do
    # put :update, id: @account, account: {  }
    # assert_redirected_to account_path(assigns(:account))
  end

  test "should destroy account" do
    # assert_difference('Account.count', -1) do
      # delete :destroy, id: @account
    # end

    # assert_redirected_to accounts_path
  end
end
