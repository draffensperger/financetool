require File.dirname(__FILE__) + "/../test_helper"

class UserFlowsTest < ActionDispatch::IntegrationTest
  fixtures :all
 
  def test_new_signup_has_default_accounts
    post user_registration_path, user: {:email => 'test124@test.com', \
      :password => 'test1234', :password_confirmation => 'test1234'}
    
    assert_redirected_to :index
    
    get accounts_path
    assert_equal(2, assigns(:accounts).length)        
  end
end