require File.dirname(__FILE__) + "/../test_helper"

class UserTest < ActiveSupport::TestCase
	fixtures :all
	
  # test "the truth" do
  #   assert true
  # end
  
  def test_adds_default_accounts_before_save
    u = User.new
    u.email = 'test@test.com'
    u.password = 'test1234'
    assert_equal(0, u.accounts.length)
    result = u.save
    assert_equal(2, u.accounts.length)
  end
	
	test "new user has default accounts" do	
		a = User.all
		assert_equal(5, a.length)
		
		u = User.new
		assert_equal(0, u.accounts.length)
		
		u.add_default_accounts
		
		assert_equal(2, u.accounts.length)
		
		if u.accounts[0].name == "Default Bank"
		  bank_index = 0
		  budget_index = 1
		else
		  bank_index = 1
      budget_index = 0		  
		end
		
		assert_equal(u.accounts[bank_index].name, "Default Bank")
    assert_equal(u.accounts[bank_index].account_type, account_types(:bank))
    assert_equal(u.accounts[bank_index].user, u)
		
		assert_equal(u.accounts[budget_index].name, "Default Budget")
		assert_equal(u.accounts[budget_index].account_type, account_types(:budget))		
		assert_equal(u.accounts[budget_index].user, u)
	end
	
	def test_token_login
		
	end
end
