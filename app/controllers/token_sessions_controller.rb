class TokenSessionsController < Devise::SessionsController
	skip_before_filter :verify_authenticity_token

	def create
		email = params[:email]
		password = params[:password]
		if not email.nil? and not password.nil?
			@user=User.find_by_email(email.downcase)
			@user.ensure_authentication_token!
      if @user.valid_password?(password)
      	 render :status=>200, :json=>{:auth_token=>@user.authentication_token}
      	 return
      end
		end
		render :status=>401, :json=>{:message=>"Invalid login."}
   end
 
  def destroy
    @user=User.find_by_authentication_token(params[:auth_token])
    if @user.nil?
      render :status=>404, :json=>{:message=>"Invalid token."}
    else
      @user.reset_authentication_token!
      render :status=>200, :json=>{:auth_token=>params[:auth_token]}
    end
  end
end
