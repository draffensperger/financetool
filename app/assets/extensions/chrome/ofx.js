// Generic helper functions that aren't related to OFX per se.
var Util = function () {
	function getHTTPObject() {
		var http = false;
		if (typeof XMLHttpRequest == 'undefined') {
			try { http = new ActiveXObject('Msxml2.XMLHTTP.6.0'); }
				catch (e) {}
			try { http = new ActiveXObject('Msxml2.XMLHTTP.3.0'); }
				catch (e) {}
			//Microsoft.XMLHTTP points to Msxml2.XMLHTTP and is redundant
			try { http = new ActiveXObject('Microsoft.XMLHTTP'); }
				catch (e) {}
		} else if (window.XMLHttpRequest) {
			try {http = new XMLHttpRequest();}
				catch (e) {}
		}
		return http;
	}

	function httpRequest(callback, verb, url, query, headers) {
		headers = headers || {};
	
		var xhttp = getHTTPObject();
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4) {
				callback(xhttp.responseText, xhttp.status, xhttp.statusText);
			}
		};
		xhttp.open(verb, url, true);
		
		var headerName;
		for (headerName in headers) {
			xhttp.setRequestHeader(headerName, headers[headerName]);
		}		
		
		xhttp.send(query);
	}	
	
	function makeStr(val) {
		if (typeof val == 'undefined') {
			return '';
		}	else if (val == null) {
			return 'null';
		} else {
			return val.toString()
		}
	}
	
	var htmlReplacements = [
		[/</g, '&lt;'],
		[/>/g, '&gt;'],
		[/\r\n/g, '<br/>'],
		[/\r/g, '<br/>'],
		[/\n/g, '<br/>'],
		[/\'/g, '&#39;'],
		[/\"/g, '&#34;'],
	];	
	function htmlEncode(txt) {		
		var len = htmlReplacements.length;
		txt = makeStr(txt);
		for (var i = 0; i < len; i++) {	
			txt = txt.replace(htmlReplacements[i][0], htmlReplacements[i][1]);
		}		
		return txt;	
	}	
	
	var S1 = function () {
		return Math.floor(
			Math.random() * 0x10 /* 65536 */
		).toString(16);
  };	
	var S4 = function () {
		return '' + S1() + S1() + S1() + S1();
	};	
	var randomGUID = function() {
		return (
			S4() + S4() + '-' +
			S4() + '-' +
			S4() + '-' +
			S4() + '-' +
			S4() + S4() + S4()
		).toUpperCase();
	};
	
	function fillInTemplate(template, obj) {
		var out = template;
		for (var key in obj) {
			var regExp = new RegExp('\\{\\{' + key + '\\}\\}', 'g');
						
			var val = obj[key];						
			val = makeStr(val).replace('{{', '\\{\\{').replace('}}', '\\}\\}');
			
			out = out.replace(regExp, val);
		}
		return out;
	}
	
	function extractAttribList(objs, attrib) {
		var attribs = [];
		var len = objs.length;
		for (var i = 0; i < len; i++) {
			attribs.push(objs[i][attrib]);
		}
		return attribs;
	}
	
	function makeMap(objs, keyAttrib) {
		var map = {};		
		var len = objs.length;
		for (var i = 0; i < len; i++) {
			map[objs[i][keyAttrib]] = objs[i];
		}
		return map;
	}
	
	return {
		httpRequest: httpRequest,
		htmlEncode: htmlEncode,
		randomGUID: randomGUID,		
		fillInTemplate: fillInTemplate,
		extractAttribList: extractAttribList,	
		makeMap: makeMap
	};
}();

// Depends on jQuery
var UIHelper = function($) {
	function populateSelect(selectJQueryObj, objs, valueAttrib, nameAttrib) {
		var htmlLines = ['<option value=""></option>'];	
		for (var i = 0; i < objs.length; i++) {			
			htmlLines.push(
				'<option value="'+ Util.htmlEncode(objs[i][valueAttrib]) +'">'+ 
					Util.htmlEncode(objs[i][nameAttrib]) +'</option>');
		}
		htmlLines.push('</select>');	
		selectJQueryObj.html(htmlLines.join(''));
	}
	
	return {
		populateSelect: populateSelect,
	};
}($);

var SGMLParser = function() {
	function getAttribVal(attrib, sgml) {		
		var attribRE = new RegExp('<' + attrib + '>(.*?)(<|$)', 'gi');
		var match = attribRE.exec(sgml);
		return match == null ? null : match[1];
	}
	
	function getSectionsAsList(sectionTag, sgml) {
		var re = new RegExp('<' + sectionTag + '>(.*?)</' + sectionTag + '>', 'gi');		
		var sections = [];		
		var match;		
		while ((match = re.exec(sgml)) != null) {
			sections.push(match[1]);
		}		
		return sections;
	}
	
	function getObjFromSection(attribList, sgml, converters) {
		converters = converters || {};
		var obj = {};
		for (var j = 0; j < attribList.length; j++) {
			var attrib = attribList[j];
			var str = getAttribVal(attrib, sgml);
			
			if (attrib in converters) {
				obj[attrib] = converters[attrib].call(undefined, str);
			} else {
				obj[attrib] = str;
			}
		}
		return obj;
	}
	
	function getSectionsAsObjList(sectionTag, attribList, sgml, converters) {	
		var objs = [];
		var sections = getSectionsAsList(sectionTag, sgml);
		for (var i = 0; i < sections.length; i++) {			
			objs.push(getObjFromSection(attribList, sections[i], converters));
		}
		
		return objs;
	}
	
	return {
		getSectionsAsList: getSectionsAsList,
		getAttribVal: getAttribVal,
		getObjFromSection: getObjFromSection,
		getSectionsAsObjList: getSectionsAsObjList
	};
}();

var OFX = function() {
	///////////////////////////////////////////
	// OFX Format templates
	///////////////////////////////////////////
	
	var HEADER = [		
		'OFXHEADER:100\r\n',
		'DATA:OFXSGML\r\n',
		'VERSION:102\r\n',
		'SECURITY:NONE\r\n',
		'ENCODING:USASCII\r\n',
		'CHARSET:1252\r\n',
		'COMPRESSION:NONE\r\n',
		'OLDFILEUID:NONE\r\n',
		'NEWFILEUID:{{newFileUID}}\r\n\r\n'].join('');

	var START = '<OFX>';

	var SIGNON = [
		'<SIGNONMSGSRQV1>',
			'<SONRQ>',
			 '<DTCLIENT>{{dtClient}}',
			 '<USERID>{{userId}}',
			 '<USERPASS>{{userPass}}',
			 '<LANGUAGE>ENG',
			 '<FI>',
				'<ORG>{{org}}',
				'<FID>{{fid}}',
			 '</FI>',
			 '<APPID>{{appId}}',
			 '<APPVER>{{appVer}}',
			'</SONRQ>',
		'</SIGNONMSGSRQV1>'
	].join('');

	var ACCTINFOTRNRQ = [
		 '<SIGNUPMSGSRQV1>',
			'<ACCTINFOTRNRQ>',
			 '<TRNUID>{{trnUID}}',
			 '<ACCTINFORQ>',
				'<DTACCTUP>19700101', 
			 '</ACCTINFORQ>',
			'</ACCTINFOTRNRQ>',
		 '</SIGNUPMSGSRQV1>',
	].join('');
	
	var BANK_STMTTRNRQ = [
		'<BANKMSGSRQV1>',
			'<STMTTRNRQ>',
			'<TRNUID>{{trnUID}}',
			'<CLTCOOKIE>4',
				'<STMTRQ>',
				'<BANKACCTFROM>',
					'<BANKID>{{org}}',
					'<ACCTID>{{acctId}}',
					'<ACCTTYPE>{{acctType}}',
				'</BANKACCTFROM>',
				'<INCTRAN>',
					'<DTSTART>{{dtStart}}',
					'<INCLUDE>Y',
				'</INCTRAN>',
				'</STMTRQ>',
			'</STMTTRNRQ>',
		'</BANKMSGSRQV1>',
	].join('');
		
	var CC_STMTTRNRQ = [
		'<CREDITCARDMSGSRQV1>',
			'<CCSTMTTRNRQ>',
			'<TRNUID>{{trnUID}}',
			'<CLTCOOKIE>4',
				'<CCSTMTRQ>',
				'<CCACCTFROM>',					
					'<ACCTID>{{acctId}}',					
				'</CCACCTFROM>',
				'<INCTRAN>',
					'<DTSTART>{{dtStart}}',
					'<INCLUDE>Y',
				'</INCTRAN>',
				'</CCSTMTRQ>',
			'</CCSTMTTRNRQ>',
		'</CREDITCARDMSGSRQV1>',
	].join('');

	var END = '</OFX>';
	
	///////////////////////////////////////////
	// Global properties and accessors
	///////////////////////////////////////////	
	
	this.appId = 'QWIN';
	this.appVer = '1800';
	var that = this;
	
	function setAppInfo(appId, appVer) {
		that.appId = appId;
		that.appVer = appVer;
	}
	
	function getAppInfo() {
		return {appId: that.appId, appVer: that.appVer};
	}
	
	///////////////////////////////////////////
	// Helper Functions
	///////////////////////////////////////////	

	function padZero(x) {
		if (x < 10) {
			return '0' + x;
		}
		return x;
	}
	function formatTime(d) {
		var datePart = '' + d.getFullYear() + padZero(d.getMonth() + 1) 
			+ padZero(d.getDate());
		
		var timePart = '' + padZero(d.getHours()) + padZero(d.getMinutes())
			+ padZero(d.getSeconds())
			
		if (timePart == '000000') {
			return datePart;
		} else {
			return datePart + timePart;
		}	
	}	
	
	function parseTime(ofxTime) {
		return new Date(
			parseInt(ofxTime.substring(0, 4)), 
			parseInt(ofxTime.substring(4, 6)) - 1,
			parseInt(ofxTime.substring(6, 8)),
			parseInt(ofxTime.substring(8, 10)),
			parseInt(ofxTime.substring(10, 12)),
			parseInt(ofxTime.substring(12, 14)),
			0);		
	}	
	
	function makeParsedCallback(callback, parseFn) {
		return function (body, status, statusText) {
			var errObj = {
					status: status, 
					statusText: statusText, 
					body: body,
					ofxErr: null,					
			};
			
			if (status == 200) {
				var ofxStatusList = SGMLParser.getSectionsAsObjList('STATUS', 
					['code', 'severity', 'message'], body);
								
				for (var i = 0; i < ofxStatusList.length; i++) {
					if (ofxStatusList[i].severity != 'INFO') {
						errObj.ofxErr = ofxStatusList[i];
						break;
					}
				}
				
				if (ofxStatusList.length > 0) {
					if (errObj.ofxErr == null) {
						callback(parseFn(body));
					} else {
						callback(parseFn(body), errObj);
					}					
				} else {
					callback(null, errObj);
				}
			} else {			
				callback(null, errObj);
			}
		};
	}
	
	var generalRequest = function(callback, fiInfo, loginInfo, 
		parseFn, ofxTemplate, account, extraSettings) {
		
		if (!/^https:\/\/.+/.test(fiInfo.url)) {
			throw 'Must use HTTPS';
		}
		
		extraSettings = extraSettings || {};
		
		var settings = extraSettings;
		settings.newFileUID = Util.randomGUID();
		settings.userId = loginInfo.userId;
		settings.userPass = loginInfo.userPass;
		settings.dtClient =  formatTime(new Date());
		settings.org = fiInfo.org;
		settings.fid = fiInfo.fid;
		settings.trnUID = settings.newFileUID;
		settings.appId = that.appId;
		settings.appVer = that.appVer;
		
		if (account) {
			settings.acctId = account.acctId;
			settings.acctType = account.acctType;
		}
		
		var requestBody = Util.fillInTemplate(
			HEADER + START + SIGNON + ofxTemplate + END, settings);		
		
		var headers = {
			'Content-Type': 'application/x-ofx;charset=utf-8', 
			'Accept': '*/*, application/x-ofx'
		};
		
		Util.httpRequest(makeParsedCallback(callback, parseFn), 
			'POST', fiInfo.url, requestBody, headers);
	}
	
	///////////////////////////////////////////
	// Public Interface Functions
	///////////////////////////////////////////
	
	function parseAccountsList(ofx) {	
		var accountAttribs = ['desc', 'bankId', 'acctId', 
			'acctType', 'supTxDl', 'svcStatus'];
		
		var sections = SGMLParser.getSectionsAsList('ACCTINFO', ofx);
		
		var accounts = [];
		for (var i = 0; i < sections.length; i++) {
			var sec = sections[i];
			var account = SGMLParser.getObjFromSection(accountAttribs, sec);
			if (sec.indexOf("BANKACCTINFO") !== -1) {
				account.stmtType = 'BANK';
			} else if (sec.indexOf("CCACCTINFO") !== -1) {
				account.stmtType = 'CREDITCARD';
			} else {
				account.stmtType = null;
			}
			accounts.push(account);
		}
		return accounts;
	}
		
	function requestAccountsList(callback, fiInfo, loginInfo) {
		generalRequest(callback, fiInfo, loginInfo, parseAccountsList, 
			ACCTINFOTRNRQ);
	}
	
	function parseBalance(sectionTag, ofx) {
		return SGMLParser.getSectionsAsObjList(
			sectionTag, ['balAmt', 'dtAsOf'], ofx, 
			{balAmt: parseFloat, dtAsOf: parseTime}
		)[0];		
	}	
	function parseTransAndBal(ofx) {
		return {
			ledgerBal: parseBalance('LEDGERBAL', ofx),
			availBal: parseBalance('AVAILBAL', ofx),
			trans: SGMLParser.getSectionsAsObjList(
				'STMTTRN', ['trnType', 'dtPosted', 'trnAmt', 'fitId', 'name'],
				ofx,
				{dtPosted: parseTime, trnAmt: parseFloat})
		};
	}
	
	function requestTransAndBal(callback, fiInfo, loginInfo, account, dtStart) {		
		var template = '';
		if (account.stmtType == 'BANK') {
			template = BANK_STMTTRNRQ;
		} else if (account.stmtType == 'CREDITCARD') {
			template = CC_STMTTRNRQ;
		}
		
		generalRequest(callback, fiInfo, loginInfo, parseTransAndBal, template,
			account, {dtStart: formatTime(dtStart)});
	}	
	
	// Export the public functions	
	
	return {
		setAppInfo: setAppInfo,
		getAppInfo: getAppInfo,
		requestAccountsList: requestAccountsList,	
		requestTransAndBal: requestTransAndBal,
		parseAccountsList: parseAccountsList,
		parseTransAndBal: parseTransAndBal,
		formatTime: formatTime,
		parseTime: parseTime
	};
}();