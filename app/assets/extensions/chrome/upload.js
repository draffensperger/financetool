$(function() {	
	var financialOrgs = [
		{id: 1, name: 'Chase (credit card)', url: 'https://ofx.chase.com', 
		 org: 'B1', fid: '8374', instructions: 'Log into the Chase website then ' +
			'click "Customer Center" then under the "Reference Center" area under ' + 
			'"Tools" click "Activate Money, Quicken, etc."'},
		{id: 2, name: 'ING DIRECT', url: 'https://ofx.ingdirect.com/OFX/ofx.html', 
		 org: 'ING DIRECT', fid: '031176110',
		 instructions: 'Sign into the ING Direct website, click "My Info" then in ' +
			'the menu below "Preferences", click on "Access Code". Create an Access ' +
			' code which will serve as your password for Direct Connect.'},
		{id: 3, name: 'Citibank', 
		 url: 'https://www.oasis.cfree.com/fip/genesis/prod/02101.ofx', 
		 org: 'ISC', fid: '2101',
		 instruction: 'You need to contact Citibank to activate ' + 
			'"Direct Connect for Quicken.", and there may be a monthly fee.'},
		{id: 4, name: 'Bank of America (All except CA, WA,&ID)', 
		 url: 'https://ofx.bankofamerica.com/cgi-forte/fortecgi?servicename=ofx_2-3&pagename=ofx', 
		 org: 'HAN', fid: '6812'},
	];
	
	var addFIRow = function() {
		$('.fiInfo').first().clone().appendTo('#fiItems');
		
		var fiName = $('.fiName').last();
		
		UIHelper.populateSelect(fiName, financialOrgs, 'id', 'name');		
		fiName.chosen({allow_single_deselect: true}).change(fiChanged);
		
		$('.loginInfo').last().hide();
		
		$('.fiInfo').last().show();
	}
	
	function removeFIRow(rowNum) {
		$('.fiInfo').eq(rowNum).remove();		
	}

	var fiChanged = function(event) {
		var val = $(this).val();
		
		if (!('oldValue' in this)) {
			this.oldValue = '';
		}		

		var row = $('.fiName').index(this);
		
		if (val == '') {			
			removeFIRow(row);
			
			if ($('.fiInfo').length == 2) {
				$('#updateButton').hide();
			}
		} else if (this.oldValue == '') {
			$('.loginInfo').eq(row).show();
			addFIRow();
			$('#updateButton').show();
		}
		
		//var fiOrg = fiOrgIdMap[val];
		
		//var orgIdHtml = htmlEnc(fiOrg.id);		
				
		// Show the hidden elements under it.		
		
		/*
		var html = 
			htmlEnc(fiOrg.name + ' Server [' + fiOrg.url + ']') + ';
		$('#ofxUrls').html(html);
		
		$('#submitFiForm' + orgIdHtml).click(function() {
			$('#fiForm' + orgIdHtml).submit();
		});
				
		$('#financeLogin').html(html);		
		*/		
		
		this.oldValue = val;
	};
	
	var accountsLoaded = function(accounts, err) {
		$('#loadingImg').hide();
		if (err) {
			var msg;
			if (err.status == 200) {
				msg = 'Invalid user/password or Direct Connect not activated.';
				
				var instructions = 'You may need to activate ' + 
					'"Direct Connect for Quicken" (and other tools) first.';
				if ('instructions' in fiOrg) {
					instructions = fiOrg.instructions;			
				}				
				$('#instructions').html(htmlEnc(instructions));						
			} else {
				msg = 'Error processeding request.';
			}
			$('#msg').html(msg);
		} else {
			var s = '<table>';
			for (var i = 0; i < accounts.length; i++) {
				var account = accounts[i];
				s += '<tr>';
				for (var attrib in account) {
					s += '<td>' + htmlEnc(attrib) + '</td><td>' + 
						 htmlEnc(account[attrib]) + '</td>';
				}
				s += '</tr>';
			}
			s += '</table>';
			$('#accountInfo').html(s);
		}
	};
	
	$('#updateButton').click(function(event) {
		$('#loadingImg').show();
		
		var loginInfo = {
			userId: $('#user').val(), userPass: $('#password').val()
		};
		OFX.requestAccountsList(accountsLoaded, fiOrg, loginInfo);				
	});
	
	var fiOrgIdMap = Util.makeMap(financialOrgs, 'id');
		
	//$("#financialNames").trigger("liszt:updated");
	
	var htmlEnc = Util.htmlEncode;	
	
	addFIRow();
	
	$( "#securityAccordian" ).accordion({ active: 0, collapsible: true, 
		heightStyle: "content"});
});