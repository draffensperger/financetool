//////////////////////////////////////////////////////////
// Test Helper Methods
//////////////////////////////////////////////////////////

function okIfMatches(expected, regExp, message) {
	regExp.lastIndex = 0;
	var msg = 'Expected to match regexp \r\n' + regExp.toString() + 
		'\r\nBut was: \r\n' + expected;
	if (typeof message !== 'undefined') {
		msg = 'Test failed: ' + message + '. ' + msg;
	}
	ok(regExp.test(expected), msg);
}

// Uses Sinon.JS
// Assumes ajaxFunction takes the callback as its first argument
function testAJAXFunction(ajaxFnThis, ajaxFunction, args, expectedRequest, 
	response, expectedCallbackArgs) {
	
	var requests = [];
	var xhr = sinon.useFakeXMLHttpRequest();
  xhr.onCreate = function (xhr) {
			requests.push(xhr);
	};
	
	var spyCallback = sinon.spy();
	
	ajaxFunction.apply(ajaxFnThis, [spyCallback].concat(args));
	
	// Test for the expected request
	equal(requests.length, 1);
	var req = requests[0];	
	if ('method' in expectedRequest) {
		equal(req.method, expectedRequest.method);
	}	
	if ('url' in expectedRequest) {
		equal(req.url, expectedRequest.url);
	}
	if('headers' in expectedRequest) {
		deepEqual(req.requestHeaders, expectedRequest.headers);
	}
	if ('requestBody' in expectedRequest) {
		var expectedReqBody = expectedRequest.requestBody;
		if (expectedReqBody instanceof RegExp) {
			okIfMatches(req.requestBody, expectedReqBody);
		} else {
			equal(req.requestBody, expectedReqBody);
		}
	}
		
	req.respond(response.status, response.headers, response.body);
	
	// Test for the expected callback args
	ok(spyCallback.args.length > 0);
	ok(spyCallback.args[0].length > 0);
  deepEqual(spyCallback.args[0], expectedCallbackArgs);
	
	xhr.restore();
}

// Assumes that the first item of args is fiInfo
function testAJAXofxFunction(ofxFunction, args, request, status, 
	response, parsedData, errorData) {	
	
	clock = sinon.useFakeTimers(TEST_DATE.getTime());	

	var protcol = 'https://'
	var url = protcol + 'sometest.com/ofx.html'
	args[0].url = url;	
	
	var expectedRequest = {
		method: 'POST', url: url, 
		headers: {
			'Content-Type': 'application/x-ofx;charset=utf-8', 
			'Accept': '*/*, application/x-ofx'
		},
		requestBody: request
	};
	
	var response = {status: status, 
		headers: { 'Content-Type': 'application/x-ofx' }, body: response};	
	
	var callbackArgs = [parsedData];
	if (errorData) {
		callbackArgs.push(errorData);
	}
	testAJAXFunction(OFX, ofxFunction, args, expectedRequest, response,
		callbackArgs);	

	// Assert an exception if just http not httpS
	args[0].url = 'http://' + url;	
	throws(function() {
		ofxFunction.apply(undefined, [function() {}].contact(args))
	}, 'Must use HTTPS');
		
	clock.restore();
}

//////////////////////////////////////////////////////////
// Util Tests
//////////////////////////////////////////////////////////

module("WebHeper");

var GUID_REG_EXP_STR = '([0-9]|[A-F]){8}(-([0-9]|[A-F]){4}){4}([0-9]|[A-F]){8}';
var GUID_REG_EXP = new RegExp('^' + GUID_REG_EXP_STR + '$');

test( 'extractAttribList', function() {
	var objs = [
		{name: 'Bank 1', url: 'https://someofx.com', org: 'B1', fid: '10898'},
		{name: 'Credit Card 1', url: 'https://testofx.com', org: 'CC', fid: '031'},
	];

	deepEqual(['Bank 1', 'Credit Card 1'], 
		Util.extractAttribList(objs, 'name'));
});

test( 'htmlencode', function() {
	equal(Util.htmlEncode(1), 1);
	equal(Util.htmlEncode(null), 'null');
	equal(Util.htmlEncode(undefined), '');
  equal(Util.htmlEncode('<div>\r\n</div>'), '&lt;div&gt;<br/>&lt;/div&gt;');
	equal(Util.htmlEncode('a\r\nb\rc\nd'), 'a<br/>b<br/>c<br/>d');	
	equal(Util.htmlEncode('Bob\'s'), 'Bob&#39;s');	
	equal(Util.htmlEncode('Bob\'s Credit Card'), 'Bob&#39;s Credit Card');	
	equal(Util.htmlEncode('"Yes"'), '&#34;Yes&#34;');
});

test( 'randomGUID', function() {
	var numIterations = 20;
	
	guidRegExp = GUID_REG_EXP;
	
	guids = {};
	for (var i = 0; i < numIterations; i++) {
		guid = Util.randomGUID();
		ok(guidRegExp.test(guid))
		ok(!(guid in guids));
		guids[guid] = true;
	}
});

test('fillInTemplate', function() {
	var obj = {
		q: 'quick',
		j: 'jumps',
		l: 'lazy',
		a1: 'over',
		n: null,
		x: 50
	};
	
	var template = 'The {{q}} brown fox {{j}} over the {{l}} dog,\r\n {{a1}} ' + 
		'and {{a1}} again, {{x}} times. n = {{n}}';
	
	equal(Util.fillInTemplate(template, obj), 
		'The quick brown fox jumps over the lazy dog,\r\n over ' + 
			'and over again, 50 times. n = null');
	
	// Have {{ and }} escaped in values to avoid exploding the value.
	// Since braces should not be escaped.
	var obj2 = {
		q: '{quick',
		j: 'jumps}',
		l: '{{q}}',
		a1: 'over}}',
		x: 50
	};	
	equal(Util.fillInTemplate(template, obj2), 
		'The {quick brown fox jumps} over the \\{\\{q\\}\\} dog,\r\n over\\}\\} ' +
			'and over\\}\\} again, 50 times. n = {{n}}');
			
	var obj3 = {a: 'a', b: undefined};
	equal(Util.fillInTemplate('This is {{a}} test of undefined: {{b}}', obj3), 
		'This is a test of undefined: ');
});

test('makeMap', function() {	
	var objs = [
		{id: 1, name: 'Obj 1', url: 'https://obj1.com'},
		{id: 2, name: 'Obj B', url: 'https://objsecond.com'},
	];
	deepEqual(Util.makeMap(objs, 'id'), {1 : objs[0], 2: objs[1]});
});

//////////////////////////////////////////////////////////
// UI Helper tests
//////////////////////////////////////////////////////////

module("UIHelper");

test( 'populateSelect', function() {
	var objs = [
		{name: 'Bank <1', url: 'https://someofx.com', org: 'B1', fid: '10898'},
		{name: 'Bob\'s Credit Card', url: '<not found>', org: 'CC', fid: '031'},
	];
	
	equal($('#testSelect').html(), '');
	
	UIHelper.populateSelect($('#testSelect'), objs, 'url', 'name');
	
	var expectedHTML = [
		'<option value=""></option>',
		'<option value="https://someofx.com">Bank &lt;1</option>',
		'<option value="&lt;not found&gt;">Bob\'s Credit Card</option>',
	].join('');	
	
	equal($('#testSelect').html(), expectedHTML);
});

//////////////////////////////////////////////////////////
// SGML Parser tests
//////////////////////////////////////////////////////////

module("SGMLParser");

test('getAttribVal', function() {
	var ofx = [
		'<DESC>Checking',
		'<BANKACCTINFO>',
			'<BANKACCTFROM>',
				'<BANKID>041279179',
				'<ACCTID>245347859',
				'<ACCTTYPE>CHECKING',
			'</BANKACCTFROM>',
			'<SUPTXDL>Y',
			'<XFERSRC>N',
			'<XFERDEST>N',
			'<SVCSTATUS>ACTIVE',
		'</BANKACCTINFO>'
		].join('');
		
	equal(SGMLParser.getAttribVal('bankId', ofx), '041279179');
	equal(SGMLParser.getAttribVal('acctId', ofx), '245347859');
	equal(SGMLParser.getAttribVal('desc', ofx), 'Checking');
	equal(SGMLParser.getAttribVal('xferSrc', ofx), 'N');
});

test('getSectionsAsList', function() {
	var sgml = [
			'<B><A>1</A><A>2</A><A>3</A><B><C>4</C>'
		].join('');
		
	deepEqual(SGMLParser.getSectionsAsList('a', sgml), ['1','2','3']);
});

test('getObjFromSection', function() {
	var sgml = '<A><V1>1<V2>a</A>'
	var converters = {v1: function(x) {return parseInt(x) + 1}};
	
	deepEqual(SGMLParser.getObjFromSection(['v1', 'v2'], sgml), 
		{v1: '1', v2: 'a'});
		
	deepEqual(SGMLParser.getObjFromSection(['v1', 'v2'], sgml, converters), 
		{v1: 2, v2: 'a'});
});

test('getSectionsAsObjList', function() {
	var sgml = [
			'<B>',
			'<A><V1>1<V2>a</A>',
			'<A><V1>2<V2>b<V3>asdjf</A>',
			'<A><V1>4<V2>d</A>',
			'</B><C>4</C>'
		].join('');
		
	deepEqual(SGMLParser.getSectionsAsObjList('a', ['v1', 'v2'], sgml), 
		[{v1: '1', v2: 'a'}, 
		 {v1: '2', v2: 'b'},
		 {v1: '4', v2: 'd'},
		]);
});

test('getSectionsAsObjList converters', function() {
	var sgml = [
			'<B>',
			'<A><V1>1<V2>a</A>',
			'<A><V1>2<V2>b<V3>asdjf</A>',
			'<A><V1>4<V2>d</A>',
			'</B><C>4</C>'
		].join('');
		
	var converters = {
		v1: function(x) {return parseInt(x) + 1},
		v2: function(s) {return s.toUpperCase()}
	};
		
	deepEqual(
		SGMLParser.getSectionsAsObjList('a', ['v1', 'v2'], sgml, converters), 
		[{v1: 2, v2: 'A'}, 
		 {v1: 3, v2: 'B'},
		 {v1: 5, v2: 'D'},
		]);
});

//////////////////////////////////////////////////////////
// OFX Tests
//////////////////////////////////////////////////////////

var TEST_APP_ID = 'TestApp';
var TEST_APP_VER = '1000';

module("OFX", {
	setup: function () {
		OFX.setAppInfo(TEST_APP_ID, TEST_APP_VER);
	}
});

var TEST_DATE = new Date('September 5, 2012 5:03:11');

var REQUEST_PREFIX_REGEXP_STR = [	
	'OFXHEADER:100\r\n',
	'DATA:OFXSGML\r\n',
	'VERSION:102\r\n',
	'SECURITY:NONE\r\n',
	'ENCODING:USASCII\r\n',
	'CHARSET:1252\r\n',
	'COMPRESSION:NONE\r\n',
	'OLDFILEUID:NONE\r\n',
	'NEWFILEUID:' + GUID_REG_EXP_STR + '\r\n',
	'\r\n',
].join("");

var RESPONSE_PREFIX_STR = [
	'OFXHEADER:100O\r\n',
	'DATA:OFXSGML\r\n',
	'VERSION:102\r\n',
	'SECURITY:NONE\r\n',
	'ENCODING:USASCII\r\n',
	'CHARSET:1252\r\n',
	'COMPRESSION:NONE\r\n',
	'OLDFILEUID:NONE\r\n',
	'NEWFILEUID:d3373f8e-1fc5-461a-b5e1-c8f31d18fca9\r\n',
	'\r\n',
].join("");

var ACCOUNTS_REQUEST_REGEXP = new RegExp(['^',
	REQUEST_PREFIX_REGEXP_STR,
	'<OFX>',
		'<SIGNONMSGSRQV1>',
			'<SONRQ>',
				'<DTCLIENT>20120905050311',
				'<USERID>myuser',
				'<USERPASS>mypass',
				'<LANGUAGE>ENG',
				'<FI>',
					'<ORG>Test Org',
					'<FID>041279179',
				'</FI>',
				'<APPID>' + TEST_APP_ID,
				'<APPVER>' + TEST_APP_VER,
			'</SONRQ>',
		'</SIGNONMSGSRQV1>',
		'<SIGNUPMSGSRQV1>',
			'<ACCTINFOTRNRQ>',
				'<TRNUID>' + GUID_REG_EXP_STR + '',
				'<ACCTINFORQ>',
					'<DTACCTUP>19700101', 
				'</ACCTINFORQ>',
			'</ACCTINFOTRNRQ>',
		'</SIGNUPMSGSRQV1>',
	'</OFX>',
	'$',
].join(''), 'g');

var BANK_ACCOUNTS_RESPONSE = [
	RESPONSE_PREFIX_STR,
	'<OFX>',
	'<SIGNONMSGSRSV1>',
		'<SONRS>',
		'<STATUS><CODE>0<SEVERITY>INFO</STATUS>',
		'<DTSERVER>20130108002837.590<LANGUAGE>ENG<DTACCTUP>20130108002837.590',
		'<FI><ORG>Test Org<FID>041279179</FI>',
		'</SONRS>',
	'</SIGNONMSGSRSV1>',
	'<SIGNUPMSGSRSV1>',
	'<ACCTINFOTRNRS>',
		'<TRNUID>58DA8D00-AF46-2080-0643-6F8D176FA232',
		'<STATUS><CODE>0<SEVERITY>INFO</STATUS>',
		'<ACCTINFORS>',
			'<DTACCTUP>19700101000000.000',
			'<ACCTINFO>',
				'<DESC>Checking',
				'<BANKACCTINFO>',
					'<BANKACCTFROM>',
						'<BANKID>041279179',
						'<ACCTID>245347859',
						'<ACCTTYPE>CHECKING',
					'</BANKACCTFROM>',
					'<SUPTXDL>Y',
					'<XFERSRC>N',
					'<XFERDEST>N',
					'<SVCSTATUS>ACTIVE',
				'</BANKACCTINFO>',
			'</ACCTINFO>',
			'<ACCTINFO>',
				'<DESC>Savings',
				'<BANKACCTINFO>',
					'<BANKACCTFROM>',
						'<BANKID>091478131',
						'<ACCTID>641864998',
						'<ACCTTYPE>SAVINGS',
					'</BANKACCTFROM>',
					'<SUPTXDL>N',
					'<XFERSRC>N',
					'<XFERDEST>N',
					'<SVCSTATUS>PEND',
				'</BANKACCTINFO>',
			'</ACCTINFO>',
		'</ACCTINFORS>',
	'</ACCTINFOTRNRS>',
	'</SIGNUPMSGSRSV1>',
	'</OFX>',
].join('');

var BANK_ACCOUNTS_DATA = [
	{
		stmtType: 'BANK',
		desc: 'Checking',
		bankId: '041279179',
		acctId: '245347859',
		acctType: 'CHECKING',
		supTxDl: 'Y',
		svcStatus: 'ACTIVE'
	},
	{
		stmtType: 'BANK',
		desc: 'Savings',
		bankId: '091478131',
		acctId: '641864998',
		acctType: 'SAVINGS',
		supTxDl: 'N',
		svcStatus: 'PEND'
	}
];

var CC_ACCOUNTS_RESPONSE = [
	RESPONSE_PREFIX_STR,
	'<OFX>',
	'<SIGNONMSGSRSV1>',
		'<SONRS>',
			'<STATUS>',
				'<CODE>0',
				'<SEVERITY>INFO',
				'<MESSAGE>SUCCESS',
			'</STATUS>',
			'<DTSERVER>20130127091441.467[-5:EST]',
			'<LANGUAGE>ENG',
			'<FI>',
				'<ORG>B1',
				'<FID>8374',
			'</FI>',
		'</SONRS>',
	'</SIGNONMSGSRSV1>',
	'<SIGNUPMSGSRSV1>',
		'<ACCTINFOTRNRS>',
			'<TRNUID>1045A9BA-519F-940B-D72A-33364491B37A',
			'<STATUS>',
				'<CODE>0',
				'<SEVERITY>INFO',
			'</STATUS>',
		'<ACCTINFORS>',
			'<DTACCTUP>20130127091441.618[-5:EST]',
			'<ACCTINFO>',
				'<DESC>CREDIT CARD',
				'<CCACCTINFO>',
					'<CCACCTFROM>',
						'<ACCTID>6130723843321559',
					'</CCACCTFROM>',
					'<SUPTXDL>Y',
					'<XFERSRC>N',
					'<XFERDEST>N',
					'<SVCSTATUS>ACTIVE',
				'</CCACCTINFO>',
			'</ACCTINFO>',
		'</ACCTINFORS>',
		'</ACCTINFOTRNRS>',
	'</SIGNUPMSGSRSV1>',
	'</OFX>',
].join('');

var CC_ACCOUNTS_DATA = [
	{
		stmtType: 'CREDITCARD',
		desc: 'CREDIT CARD',
		bankId: null,
		acctId: '6130723843321559',
		acctType: null,
		supTxDl: 'Y',
		svcStatus: 'ACTIVE'
	}
];

var BANK_TRANS_REQUEST = new RegExp(['^',
	REQUEST_PREFIX_REGEXP_STR,
	'<OFX>',
		'<SIGNONMSGSRQV1>',
			'<SONRQ>',
				'<DTCLIENT>20120905050311',
				'<USERID>myuser',
				'<USERPASS>mypass',
				'<LANGUAGE>ENG',
				'<FI>',
					'<ORG>Test Org',
					'<FID>041279179',
				'</FI>',
				'<APPID>' + TEST_APP_ID,
				'<APPVER>' + TEST_APP_VER,
			'</SONRQ>',
		'</SIGNONMSGSRQV1>',
		'<BANKMSGSRQV1>',
			'<STMTTRNRQ>',
			'<TRNUID>' + GUID_REG_EXP_STR,
			'<CLTCOOKIE>4',
				'<STMTRQ>',
				'<BANKACCTFROM>',
					'<BANKID>Test Org',
					'<ACCTID>205271253',
					'<ACCTTYPE>CHECKING',
				'</BANKACCTFROM>',
				'<INCTRAN>',
					'<DTSTART>20121202',
					'<INCLUDE>Y',
				'</INCTRAN>',
				'</STMTRQ>',
			'</STMTTRNRQ>',
		'</BANKMSGSRQV1>',
	'</OFX>',
	'$'
].join(''), 'g');

var BANK_TRANS_RESPONSE = [
	RESPONSE_PREFIX_STR,
	'<OFX>',
		'<SIGNONMSGSRSV1>',
			'<SONRS>',
				'<STATUS>',
					'<CODE>0',
					'<SEVERITY>INFO',
				'</STATUS>',
				'<DTSERVER>20130101231322.315',
				'<LANGUAGE>ENG',
				'<DTACCTUP>20130101231322.315',
				'<FI>',
					'<ORG>Test Org',
					'<FID>041279179',
				'</FI>',
			'</SONRS>',
		'</SIGNONMSGSRSV1>',
		'<BANKMSGSRSV1>',
			'<STMTTRNRS>',
				'<TRNUID>c22bbb99-aa82-4dca-9e0d-7475f919bb70',
				'<STATUS>',
					'<CODE>0',
					'<SEVERITY>INFO',
				'</STATUS>',
				'<STMTRS>',
					'<CURDEF>USD',
					'<BANKACCTFROM>',
						'<BANKID>041279179',
						'<ACCTID>205271253',
						'<ACCTTYPE>CHECKING',
					'</BANKACCTFROM>',
					'<BANKTRANLIST>',
						'<DTSTART>20121202000000.000',
						'<STMTTRN>',
							'<TRNTYPE>DEBIT',
							'<DTPOSTED>20121201015928.000',
							'<TRNAMT>-6.19',
							'<FITID>568',
							'<NAME>Debit Card Purchase - SOME SHOP SOMEWHERE',
						'</STMTTRN>',
						'<STMTTRN>',
							'<TRNTYPE>CREDIT',
							'<DTPOSTED>20121130161927.000',
							'<TRNAMT>0.83',
							'<FITID>567',
							'<NAME>Here\'s your interest!',
						'</STMTTRN>',
					'</BANKTRANLIST>',
					'<LEDGERBAL>',
						'<BALAMT>4341.62',
						'<DTASOF>20130101231324.872',
					'</LEDGERBAL>',
					'<AVAILBAL>',
						'<BALAMT>3906.63',
						'<DTASOF>20130101231324.872',
					'</AVAILBAL>',
				'</STMTRS>',
			'</STMTTRNRS>',
		'</BANKMSGSRSV1>',
	'</OFX>',
].join('');

var BANK_TRANS_AND_BAL_OBJ = {
	ledgerBal: {balAmt: 4341.62, dtAsOf: new Date('January 1, 2013 23:13:24')},
	availBal: {balAmt: 3906.63, dtAsOf: new Date('January 1, 2013 23:13:24')},
	trans: [
		{trnType: 'DEBIT', dtPosted: new Date('December 01, 2012 01:59:28'), 
			trnAmt: -6.19, fitId: '568', 
			name: 'Debit Card Purchase - SOME SHOP SOMEWHERE'}, 
		{trnType: 'CREDIT', dtPosted: new Date('November 30, 2012 16:19:27'),
			trnAmt: 0.83, fitId: '567', 
			name: 'Here\'s your interest!'}, 
	]
};

var CC_TRANS_REQUEST = new RegExp(['^',
	REQUEST_PREFIX_REGEXP_STR,
	'<OFX>',
		'<SIGNONMSGSRQV1>',
			'<SONRQ>',
				'<DTCLIENT>20120905050311',
				'<USERID>credituser',
				'<USERPASS>creditpass',
				'<LANGUAGE>ENG',
				'<FI>',
					'<ORG>CC134',
					'<FID>48291',
				'</FI>',
				'<APPID>' + TEST_APP_ID,
				'<APPVER>' + TEST_APP_VER,
			'</SONRQ>',
		'</SIGNONMSGSRQV1>',
		'<CREDITCARDMSGSRQV1>',
			'<CCSTMTTRNRQ>',
				'<TRNUID>' + GUID_REG_EXP_STR,
				'<CLTCOOKIE>4',
				'<CCSTMTRQ>',
					'<CCACCTFROM>',
					'<ACCTID>6391132916521237',
					'</CCACCTFROM>',
					'<INCTRAN>',
						'<DTSTART>20130101',						
						'<INCLUDE>Y',
					'</INCTRAN>',
				'</CCSTMTRQ>',
			'</CCSTMTTRNRQ>',
		'</CREDITCARDMSGSRQV1>',
	'</OFX>',
	'$',
].join(""), 'g');

var CC_TRANS_RESPONSE = [
	RESPONSE_PREFIX_STR,
	'<OFX>',
		'<SIGNONMSGSRSV1>',
			'<SONRS>',
				'<STATUS>',
					'<CODE>0',
					'<SEVERITY>INFO',
					'<MESSAGE>SUCCESS',
				'</STATUS>',
				'<DTSERVER>20130113210344.181[-5:EST]',
				'<LANGUAGE>ENG',
				'<FI>',
					'<ORG>CC134',
					'<FID>48291',
				'</FI>',
			'</SONRS>',
		'</SIGNONMSGSRSV1>',
		'<CREDITCARDMSGSRSV1>',
			'<CCSTMTTRNRS>',
				'<TRNUID>20130113205150.000',
				'<STATUS>',
					'<CODE>0',
					'<SEVERITY>INFO',
				'</STATUS>',
				'<CLTCOOKIE>4',
				'<CCSTMTRS>',
					'<CURDEF>USD',
					'<CCACCTFROM>',
						'<ACCTID>6391132916521237',
					'</CCACCTFROM>',
					'<BANKTRANLIST>',
						'<DTSTART>20130113150809.000[-5:EST]',
						'<DTEND>20130113155149.000[-5:EST]',						
						'<STMTTRN>',
							'<TRNTYPE>DEBIT',
							'<DTPOSTED>20130111120000[0:GMT]',
							'<TRNAMT>-31.88',
							'<FITID>2013011155432863010000073244650',
							'<NAME>VESTA  *CELL-PHONE',
						'</STMTTRN>',
						'<STMTTRN>',
							'<TRNTYPE>CREDIT',
							'<DTPOSTED>20130112120000[0:GMT]',
							'<TRNAMT>2216.15',
							'<FITID>2013011110110110900014779233878',
							'<NAME>Payment Thank You - Web',
						'</STMTTRN>',
					'</BANKTRANLIST>',
					'<LEDGERBAL>',
						'<BALAMT>-20.44',
						'<DTASOF>20130114070000.000[-5:EST]',
					'</LEDGERBAL>',
					'<AVAILBAL>',
						'<BALAMT>4360',
						'<DTASOF>20130114070000.000[-5:EST]',
					'</AVAILBAL>',
				'</CCSTMTRS>',
			'</CCSTMTTRNRS>',
		'</CREDITCARDMSGSRSV1>',
	'</OFX>',
].join("");
		
var CC_TRANS_AND_BAL_OBJ = {
	ledgerBal: {balAmt: -20.44, dtAsOf: new Date('January 14, 2013 7:00:00')},
	availBal: {balAmt: 4360, dtAsOf: new Date('January 14, 2013 7:00:00')},
	trans: [
		{trnType: 'DEBIT', dtPosted: new Date('January 11, 2013 12:00:00'), 
			trnAmt: -31.88, fitId: '2013011155432863010000073244650', 
			name: 'VESTA  *CELL-PHONE'}, 
		{trnType: 'CREDIT', dtPosted: new Date('January 12, 2013 12:00:00'),
			trnAmt: 2216.15, fitId: '2013011110110110900014779233878', 
			name: 'Payment Thank You - Web'}, 
	]
};

test('formatTime', function() {		
	equal(OFX.formatTime(TEST_DATE), '20120905050311');
	
	equal(OFX.formatTime(new Date("January 12, 2013")), '20130112');
});

test('setAppInfo', function() {		
	OFX.setAppInfo('TEST', '9999');	
	deepEqual(OFX.getAppInfo(), {appId: 'TEST', appVer: '9999'});
});

test('parseTime', function() {		
	deepEqual(OFX.parseTime('20120905050311'), TEST_DATE);
});

test('parseAccountsList', function() {	
	deepEqual(OFX.parseAccountsList(BANK_ACCOUNTS_RESPONSE), 
		BANK_ACCOUNTS_DATA);
	deepEqual(OFX.parseAccountsList(CC_ACCOUNTS_RESPONSE), 
		CC_ACCOUNTS_DATA);
});

test('requestAccountsList', function() {		
	var fiInfo = {org: 'Test Org', fid: '041279179'};
	var loginInfo = {userId: 'myuser', userPass: 'mypass'};

	testAJAXofxFunction(OFX.requestAccountsList, [fiInfo, loginInfo], 
		ACCOUNTS_REQUEST_REGEXP, 200, BANK_ACCOUNTS_RESPONSE, BANK_ACCOUNTS_DATA);
	
	testAJAXofxFunction(OFX.requestAccountsList, [fiInfo, loginInfo], 
		ACCOUNTS_REQUEST_REGEXP, 200, CC_ACCOUNTS_RESPONSE, CC_ACCOUNTS_DATA);
});

test('parseTransAndBal', function() {	
	deepEqual(OFX.parseTransAndBal(BANK_TRANS_RESPONSE), 
		BANK_TRANS_AND_BAL_OBJ);
});

test('requestTransAndBal bank', function() {		
	testAJAXofxFunction(OFX.requestTransAndBal, [
			{org: 'Test Org', fid: '041279179'}, 
			{userId: 'myuser', userPass: 'mypass'},
			{acctId: '205271253', acctType: 'CHECKING', stmtType: 'BANK'},
			new Date("December 2, 2012")
		],BANK_TRANS_REQUEST, 200, BANK_TRANS_RESPONSE, BANK_TRANS_AND_BAL_OBJ);
});

test('requestTransAndBal credit card', function() {		
	testAJAXofxFunction(OFX.requestTransAndBal, [
			{org: 'CC134', fid: '48291'}, 
			{userId: 'credituser', userPass: 'creditpass'},
			{acctId: '6391132916521237', acctType: null, stmtType: 'CREDITCARD'},
			new Date("January 1, 2013")
		],CC_TRANS_REQUEST, 200, CC_TRANS_RESPONSE, CC_TRANS_AND_BAL_OBJ);	
});

var OFX_ERR_RESPONSE = [
		RESPONSE_PREFIX_STR,
		'<OFX>',
		'<SIGNONMSGSRSV1>',
			'<SONRS>',
			'<STATUS>',
				'<CODE>2000',
				'<SEVERITY>ERROR',
				'<MESSAGE>Unable to process request.',
			'</STATUS>',
			'<DTSERVER>20130126172508.251[-5:EST]',
			'<LANGUAGE>ENG',
			'<FI>',
				'<ORG>TEST',
				'<FID>9382',
			'</FI>',
			'</SONRS>',
		'</SIGNONMSGSRSV1>',
		'<SIGNUPMSGSRSV1>',
			'<ACCTINFOTRNRS>',
				'<TRNUID>A27BA77C-A660-E3A0-A466-67EAB3E062C6',
				'<STATUS>',
					'<CODE>15500',
					'<SEVERITY>ERROR',
				'</STATUS>',
			'</ACCTINFOTRNRS>',
		'</SIGNUPMSGSRSV1>',
		'</OFX>',		
	].join('');

test('request accounts or transAndBal errors', function() {
	var fiInfo = {org: 'Test Org', fid: '041279179'};
	var loginInfo = {userId: 'myuser', userPass: 'mypass'};	
	var acctInfo = {acctId: '205271253', acctType: 'CHECKING', stmtType: 'BANK'};
	var transAndBalArgs = 
		[fiInfo, loginInfo, acctInfo, new Date("December 2, 2012")];
		
	var ofxErrorResponseObj = {
		status: 200, statusText: 'OK', body: OFX_ERR_RESPONSE, 
		 ofxErr: {code: '2000', severity: 'ERROR', 
			message: 'Unable to process request.'}
	}; 
	
	testAJAXofxFunction(OFX.requestAccountsList, [fiInfo, loginInfo], 
		ACCOUNTS_REQUEST_REGEXP, 500, 'Error', null, 
		{status: 500, statusText: 'Internal Server Error', body: 'Error', 
		 ofxErr: null});
		
	testAJAXofxFunction(OFX.requestAccountsList, [fiInfo, loginInfo], 
		ACCOUNTS_REQUEST_REGEXP, 200, OFX_ERR_RESPONSE, [], ofxErrorResponseObj);
	
	testAJAXofxFunction(OFX.requestAccountsList, [fiInfo, loginInfo], 
		ACCOUNTS_REQUEST_REGEXP, 200, 'asdf', null,
		{status: 200, statusText: 'OK', body: 'asdf', ofxErr: null});
		
  testAJAXofxFunction(OFX.requestTransAndBal, transAndBalArgs, 
		BANK_TRANS_REQUEST, 400, 'Error', null,
		{status: 400, statusText: 'Bad Request', body: 'Error', 
		 ofxErr: null});		 
		 
	testAJAXofxFunction(OFX.requestTransAndBal, transAndBalArgs, 
		BANK_TRANS_REQUEST, 200, OFX_ERR_RESPONSE, 
		{availBal: undefined, ledgerBal: undefined, trans: []}, 
		ofxErrorResponseObj);
		
	testAJAXofxFunction(OFX.requestTransAndBal, transAndBalArgs, 
		BANK_TRANS_REQUEST, 200,'asdf', null,
		{status: 200, statusText: 'OK', body: 'asdf', ofxErr: null});
});

/*

http://momentjs.com/

outline of requirement

User arrives at extension.

Puts in their FinanceTool username and password.
Logs them into FinanceTool within the extension.

If they have bank accounts set up, it displays those accounts:

[[Upload Transactions]]

ING Direct: user [    ]  password [    ]
Chase: user [    ]  password [    ]
Add Financial Institution
---------------

Chase  
  CHECKING 'Checking' (1st) => My Chase
	SAVINGS 'Savings ' (1st) => (not used)

Security Info
HTTPS - explanation
OFX - explanation
Links to view security certificates

View OFX server communication log  [LOG SHOWS IT WILL BE CLEARED WHEN BROWSER CLOSED] or button [Clear Log]

View FinanceTool communication log  [LOG SHOWS IT WILL BE CLEARED WHEN BROWSER CLOSED] or button [Clear Log]
	
*/