class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # , :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :token_authenticatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  # attr_accessible :title, :body
	
	has_many :accounts, :autosave => true
	has_many :locations, :autosave => true
	
	before_save :add_default_accounts
	
	def add_default_accounts
		default_accounts = Account.joins(:default_account)
				
    default_accounts.each do |default_account|    
      account = Account.create
      account.name = default_account.name
      default_account.attributes.each do |name, value|
        account[name] = value
      end      
           
      account.user = self
      self.accounts << account
    end
    
    return self.ensure_authentication_token	
  end
end
