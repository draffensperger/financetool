class Account < ActiveRecord::Base
  belongs_to :account_type
	belongs_to :user
  attr_protected :name
  has_one :default_account
end
