class AccountType < ActiveRecord::Base
  attr_protected :name, :show_negated, :reimb_cat_name, :reimb_cat_code
	has_many :accounts
end
